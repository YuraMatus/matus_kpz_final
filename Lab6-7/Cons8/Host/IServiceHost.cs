﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons8.Host
{
    interface IServiceHost
    {
        void AddEndpoint(Type contractInterface, string endpointAddress = null);
        void Open();
        void Close();
    }
}
