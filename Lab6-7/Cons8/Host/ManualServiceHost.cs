﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons8.Host
{
    internal class ManualServiceHost /*: IServiceHost*/
    {
        private bool isOpen;
        private readonly ServiceHost serviceHost;

        public ManualServiceHost(Type serviceType, Type contractType, Uri addressUri)
        {
            serviceHost = new ServiceHost(serviceType, addressUri);
            AddEndpoint(contractType, addressUri.ToString());
        }

        public void AddEndpoint(Type contractType, string endpointAddress = null)
        {
            string ea = endpointAddress ?? serviceHost.BaseAddresses[0].ToString();
            serviceHost.AddServiceEndpoint(contractType, new NetTcpBinding(SecurityMode.None), ea);
        }

        public void Open()
        {
            if (!isOpen)
            {
                serviceHost.Opened += (sender, args) => isOpen = true;
                serviceHost.Open();
            }
        }

        public void Close()
        {
            if (isOpen)
            {
                serviceHost.Closed += (sender, args) => isOpen = false;
                serviceHost.Close();
            }
        }
    }

    internal class DBServiceHost : ManualServiceHost
    {
        public DBServiceHost(Uri addressUri) : base(typeof(DBServiceHost), typeof(Contact.IDBService), addressUri)
        { }

        public DBServiceHost() : this(new Uri("net.ctp://localhost:25901/DBService"))
        { }
    }
}
