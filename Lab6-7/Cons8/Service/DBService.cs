﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons8.Service
{
    /*[ServiceBehaviour(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerSession]
    public class DBService : Contact.IDBService
    {
        private static readonly Dictionary<string, Contact.IDBServiceCallback> Users = new Dictionary<string, Contact.IDBServiceCallback>();
        private static readonly List<string> UserNames = new List<string>();
        private string username;

        public DataContract.SystemMEssage Login(string username)
        {
            if (!Users.ContainsKey(username))
            {
                this.username = username;
                UserNames.Add(username);
                Users.Add(username, OperationContext.Current.GetCallbackChannel<Contact.IDBServiceCallback>());
                UserListChanged(username);
                return new DataContract.SystemMEssage
                {
                    Status = DataContract.SystemMEssage.SystemMessageType.Good,
                    Message = string.Format("{0} successfully logged in.", username)
                };
            }
            else
            {
                return new DataContract.SystemMEssage
                {
                    Status = DataContract.SystemMEssage.SystemMessageType.Error,
                    Message = "User with specified username is already logged in"
                };
            }
        }

        public DataContract.SystemMEssage Logout()
        {
            if (Users.ContainsKey(username))
            {
                Users.Remove(username);
                UserNames.Remove(username);
                UserListChanged(null);
                return new DataContract.SystemMEssage
                {
                    Status = DataContract.SystemMEssage.SystemMessageType.Good,
                    Message = string.Format("{0}, successfully logged out.", username)
                };
            }
            else
            {
                return new DataContract.SystemMEssage
                {
                    Status = DataContract.SystemMEssage.SystemMessageType.Error,
                    Message = "Log in first"
                };
            }
        }

        public DataContract.SystemMEssage UpdateCharacter(DataContract.Character character)
        {
            string target = character.Owner;

            try
            {
                if (Users.ContainsKey(target))
                {
                    Users[target].ReceieveCharacter(character);
                    return new DataContract.SystemMEssage
                    {
                        Message = "Updated.",
                        Status = DataContract.SystemMEssage.SystemMessageType.Good
                    };
                }
            }
            catch
            {
                return new DataContract.SystemMEssage
                {
                    Message = "Error.",
                    Status = DataContract.SystemMEssage.SystemMessageType.Good
                };
            }
            return new DataContract.SystemMEssage
            {
                Message = "Error.",
                Status = DataContract.SystemMEssage.SystemMessageType.Good
            };
        }

        public List<string> GetLoggedInUsers() { return UserNames; }

        public static void UserListChanged(string username)
        {
            var leavedUsersList = new List<string>();
            foreach (var user in Users)
            {
                try
                {
                    if (user.Key != username)
                        user.Value.UserListChanged();
                }
                catch
                {
                    leavedUsersList.Add(user.Key);
                }
            }

            leavedUsersList.ForEach(delegate (string s)
                {
                    UserNames.Remove(s);
                    Users.Remove(s);
                });
        }
    }*/
}
