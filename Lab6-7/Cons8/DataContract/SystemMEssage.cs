﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons8.DataContract
{
    [DataContract]
    public class SystemMEssage
    {
        [DataContract]
        public enum SystemMessageType
        {
            [EnumMember] Good,
            [EnumMember] Warning,
            [EnumMember] Error
        }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public SystemMessageType Status { get; set; }

        public override string ToString()
        {
            return string.Format("{0} - {1}", Status, Message);
        }
    }
}
