﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons8.Contact
{
    [ServiceContract(CallbackContract = typeof(IDBService), SessionMode = SessionMode.Required]
    public interface IDBService
    {
        [OperationContract(IsInitiating = true, IsTerminating = false)]
        SystemMessage Login(string username);

        [OperationContract(IsInitiating = false, IsTerminating = true)]
        SystemMessage Logout();

        [OperationContract(IsInitiating = false, IsTerminating = false)]
        SystemMessage UpdateObject(DataContract.Character character);

        [OperationContract(IsInitiating = false, IsTerminating = false)]
        List<string> GetLoggedInUsers();
    }
}
