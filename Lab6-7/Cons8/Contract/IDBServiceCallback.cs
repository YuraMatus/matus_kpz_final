﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons8.Contact
{
    public interface IDBServiceCallback
    {
        [OperationContract(IsOneWay = true)]
        void ReceieveCharacter(DataContract.Character character);

        [OperationContract(IsOneWay = true)]
        void UserListChanged();
    }
}
