﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons8.Client
{
    class Client
    {
        public static DataModel.DataModel Load()
        {
            DataModel.DataModel dm = new DataModel.DataModel();
            string connectionString = "Server=KILLERQUEEN\\SQL_DATABASE; Database=Cons; Integrated Security=True";
            string queryString = "select PlayerCharacters.name, PlayerCharacters.hp, PlayerCharacters.mp, PlayerAccounts.name as owner " +
                "from PlayerCharacters join PlayerAccounts on PlayerCharacters.account_id = PlayerAccounts.id";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);

                /*
                var objects = from Characters in db.PlayerCharacters
                              join Accounts in db.PlayerAccounts on Characters.account_id equals Accounts.id
                              select new { Characters.name, Characters.hp, Characters.mp, Accounbts.name };
                */

                /*
                var objects = ctx.Database.SqlQuery("select PlayerCharacters.name, PlayerCharacters.hp, PlayerCharacters.mp, PlayerAccounts.name as owner " +
                    "from PlayerCharacters join PlayerAccounts on PlayerCharacters.account_id = PlayerAccounts.id").ToList<DataModel.Character>();
                */

                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        DataModel.Character character = new DataModel.Character();
                        character.name = (string)reader[0];
                        character.hp = Int32.Parse(string.Format("{0}", reader[1]));
                        character.mp = Int32.Parse(string.Format("{0}", reader[2]));
                        character.owner = (string)reader[3];
                        dm.characters.Add(character);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return null;
                }
            }
            return dm;
        }

        public static void AddInfo(DataModel.Character character)
        {
            string connectionString = "Server=KILLERQUEEN\\SQL_DATABASE; Database=Cons; Integrated Security=True";
            string query1String = "insert into PlayerAccounts values (" +
                "(select (case when max(id) is null then 1 else max(id) + 1 end) from PlayerAccounts), " +
                "@accountName)";

            string query2String = "insert into PlayerCharacters values (" +
                "(select (case when max(id) is null then 1 else max(id) + 1 end) from PlayerCharacters), " +
                "@characterName, @hp, @mp, " + 
                "(select id from PlayerAccounts where name = @accountName))";

            string query3String = "select id from PlayerAccounts where name = @accountName";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command1 = new SqlCommand(query1String, connection);
                command1.Parameters.AddWithValue("@accountName", '\'' + character.owner + '\'');

                SqlCommand command2 = new SqlCommand(query2String, connection);
                command2.Parameters.AddWithValue("@characterName", '\'' + character.name + '\'');
                command2.Parameters.AddWithValue("@hp", character.hp);
                command2.Parameters.AddWithValue("@mp", character.mp);
                command2.Parameters.AddWithValue("@accountName", '\'' + character.owner + '\'');

                SqlCommand command3 = new SqlCommand(query3String, connection);
                command3.Parameters.AddWithValue("@accountName", '\'' + character.owner + '\'');

                try
                {
                    connection.Open();
                    SqlDataReader reader = command3.ExecuteReader();
                    if (reader.FieldCount > 0)
                    {
                        reader.Close();
                        reader = command1.ExecuteReader();
                    }
                    reader.Close();
                    reader = command2.ExecuteReader();
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public static void RemoveInfo(DataModel.Character character)
        {
            string connectionString = "Server=KILLERQUEEN\\SQL_DATABASE; Database=Cons; Integrated Security=True";
            string query1String = "delete from PlayerAccounts where name = @accountName";
            string query2String = "delete from PlayerCharacters where name = @characterName";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command1 = new SqlCommand(query1String, connection);
                command1.Parameters.AddWithValue("@accountName", '\'' + character.owner + '\'');

                SqlCommand command2 = new SqlCommand(query2String, connection);
                command2.Parameters.AddWithValue("@characterName", '\'' + character.name + '\'');

                try
                {
                    connection.Open();
                    SqlDataReader reader = command1.ExecuteReader();
                    reader.Close();
                    reader = command2.ExecuteReader();
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public static void Clean()
        {
            string connectionString = "Server=KILLERQUEEN\\SQL_DATABASE; Database=Cons; Integrated Security=True";
            string query1String = "delete from PlayerCharacters";
            string query2String = "delete from PlayerAccounts";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command1 = new SqlCommand(query1String, connection);
                SqlCommand command2 = new SqlCommand(query2String, connection);

                try
                {
                    connection.Open();
                    SqlDataReader reader = command1.ExecuteReader();
                    reader.Close();
                    reader = command2.ExecuteReader();
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
